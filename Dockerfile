FROM node:16.14.0-alpine3.15
#RUN addgroup salon-api && adduser -S -G salon-api salon-api
#RUN mkdir /salon-api && chown salon-api:salon-api /salon-api
#USER salon-api
#RUN chmod 775 /salon-api/
WORKDIR /usr/src/app


#RUN chgrp -R salon-api /app/
COPY package*.json ./

RUN npm config set registry "http://registry.npmjs.org" --global
RUN npm config set fetch-retries 5
RUN npm config set fetch-retry-mintimeout 600000
RUN npm config set fetch-retry-maxtimeout 1200000
RUN npm config set fetch-timeout 1800000
RUN npm install --save express
COPY  . .
#RUN npm run build
#RUN addgroup -S app  &&  adduser -S -G app app  &&  chown -R app /app

#RUN npm run build
EXPOSE 8089
CMD ["node", "index.js"]




