import express  from "express";
import cors from "cors";
import session from "express-session";
import dotenv from "dotenv";
import db from "./config/Database.js";
import SequelizeStore from "connect-session-sequelize";
import UserRoute from "./routes/UserRoute.js";
import ProductRoute from "./routes/ProductRoute.js";
import AuthRoute from "./routes/AuthRoute.js";
import BranchRoute from "./routes/BranchRoute.js";
import CompanyRoute from "./routes/CompanyRoute.js";
import DiscountRoute from "./routes/DiscountRoute.js";
import EmployeeRoute from "./routes/EmployeeRoute.js";
import MemberRoute from "./routes/MemberRoute.js";
import OrderRoute from "./routes/OrderRoute.js";
import OrderDetailRoute from "./routes/OrderDetailRoute.js";
import CartRoute from "./routes/CartRoute.js";
import CategoryRoute from "./routes/CategoryRoute.js";
import ProductSalonRoute from "./routes/ProductSalonRoute.js";
import KeranjangRoute from "./routes/KeranjangRoute.js";

dotenv.config();

const app = express();

const sessionStore = SequelizeStore(session.Store);

const store = new sessionStore({
    db: db
});

//    (async() => {
//            await db.sync({force: true});
//        }) ();

app.get('/', (req, res) => {
    res.send('Hello world');
})

// app.get('/connect-db', (req, res) => {
//     const connection = mysql.createConnection({
//       host: 'mysql_docker',
//       user: 'root',
//       password: 'hazlam2020',
//       database: 'h941teledata_salon'
//     })

//     connection.connect()

//   connection.query('SELECT * FROM x941users', (err, rows, fields) => {
//     if (err) throw err

//     console.log('The solution is: ', rows)
//   })

//   connection.end()
//   res.send('Connect to db')
// })

app.use(session({
    secret: process.env.MY_SECRET_KEY,
    resave: false,
    saveUninitialized: true,
    store: store,
    cookie: {
        secure: 'auto'
    }
}))

const whitelist = ["http://127.0.0.1:8081", "http://kyoshibeauty.com:8081"]
const corsOptions = {
  origin: function (origin, callback) {
    if (!origin || whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error("Not allowed by CORS"))
    }
  },
  credentials: true,
}

app.use(cors({
    corsOptions,
    methods: ['POST', 'GET', 'PUT', 'UPDATE', 'DELETE']
}));

app.use(express.json());
app.use(AuthRoute);
app.use(BranchRoute);
app.use(CompanyRoute);
app.use(DiscountRoute);
app.use(EmployeeRoute);
app.use(MemberRoute);
app.use(UserRoute);
app.use(ProductRoute);
app.use(OrderRoute);
app.use(OrderDetailRoute);
app.use(CartRoute);
app.use(CategoryRoute);
app.use(ProductSalonRoute);
app.use(KeranjangRoute);

// store.sync(); // untuk sync table session ke database

app.listen(process.env.APP_PORT, () => {
    console.log('Server up and running');
});